import boto3
from ruamel.yaml import YAML
from crhelper import CfnResource

file_name = 'local.yaml'
tmp_path = '/tmp/'

s3 = boto3.resource('s3')
helper = CfnResource()
yaml=YAML(typ='safe')

def read_file(file_path):
    with open (file_path, 'r') as file_stream:
        return file_stream.read()

def write_yaml_file(dest_file_path, yaml_content):
    with open(dest_file_path, 'w', encoding='utf-8') as save_file:
        yaml.default_flow_style = False
        yaml.dump(yaml_content, save_file)

def write_txt_file(dest_file_path, file_content):
    with open(dest_file_path, 'w', encoding='utf-8') as save_file:
        save_file.write(file_content)

@helper.create
def create(event, _):
    region = event['ResourceProperties']['Region']
    aem_oak_run_version = event['ResourceProperties']['AemOakRunVersion']
    s3_source_bucket = event['ResourceProperties']['S3SourceBucket']
    s3_source_key = event['ResourceProperties']['S3SourceKey']
    s3_data_bucket = event['ResourceProperties']['S3DataBucket']
    aoc_stack_prefix = event['ResourceProperties']['AOCStackPrefix']

    s3.meta.client.download_file(s3_source_bucket, s3_source_key + 'scripts/' + file_name, tmp_path + file_name)

    file_content = read_file(tmp_path + file_name)
    local_yaml = yaml.load(file_content)

    local_yaml['common::aws_region'] = region
    local_yaml['aem_curator::config_aem_tools::oak_run_version'] = aem_oak_run_version

    write_yaml_file(tmp_path + file_name, local_yaml)

    s3.meta.client.upload_file(tmp_path + file_name, s3_data_bucket, aoc_stack_prefix + '/data/local.yaml')

@helper.update
def no_op(_, __):
    pass
@helper.delete
def no_op(_, __):
    pass

def handler(event, context):
    helper(event, context)
